#!/usr/bin/env python

"""Display "Hello World" to the user"""


def main():
    """Display "Hello World" to the user"""
    print("Hello World")


if __name__ == '__main__':
    main()
